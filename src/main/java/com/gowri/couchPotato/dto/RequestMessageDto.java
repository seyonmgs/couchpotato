package com.gowri.couchPotato.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gowri.couchPotato.domain.ShowsInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RequestMessageDto  {

    @Valid
    @NotNull
    @JsonProperty("payload")
    private List<ShowsInfo> listOfShows;

    @NotNull
    @JsonProperty("skip")
    @Min(value = 0)
    private Integer noOfShowsToSkip;

    @NotNull
    @JsonProperty("take")
    @Min(value = 0)
    private Integer noOfShowsToAccept;

    @NotNull
    @JsonProperty("totalRecords")
    @Min(value = 0)
    private Integer total;

}
