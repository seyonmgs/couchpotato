package com.gowri.couchPotato.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gowri.couchPotato.domain.ResponseInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResponseMessageDto implements Serializable {
    @JsonProperty("response")
    private List<ResponseInfo> responseInfoList;
}
