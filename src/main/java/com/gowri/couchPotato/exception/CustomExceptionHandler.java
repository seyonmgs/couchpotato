package com.gowri.couchPotato.exception;

import com.gowri.couchPotato.dto.ErrorMessageDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RequiredArgsConstructor
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageSource messageSource;

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        /*ErrorMessageDto errorResponseDto = new ErrorMessageDto("Could not decode request: JSON parsing failed");*/
        return new ResponseEntity<>(new ErrorMessageDto(messageSource.getMessage("jsonParsingError", null,LocaleContextHolder.getLocale()))
                , headers, HttpStatus.BAD_REQUEST);
    }


    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request){
        /*ErrorMessageDto errorResponseDto = new ErrorMessageDto("Could not decode request: JSON parsing failed");*/
        return new ResponseEntity<>(new ErrorMessageDto(messageSource.getMessage("jsonParsingError", null, LocaleContextHolder.getLocale()))
                , headers, HttpStatus.BAD_REQUEST);
    }


}
