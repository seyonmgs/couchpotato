package com.gowri.couchPotato.service;

import com.gowri.couchPotato.domain.ResponseInfo;
import com.gowri.couchPotato.domain.ShowsInfo;
import com.gowri.couchPotato.dto.RequestMessageDto;
import com.gowri.couchPotato.dto.ResponseMessageDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class AppService {

    @Autowired
    private ModelMapper modelMapper;



    public ResponseMessageDto processRequest(RequestMessageDto requestMessageDto){


        return new ResponseMessageDto(requestMessageDto
                .getListOfShows()
                .stream()
                .filter(ShowsInfo::isDrm)
                .filter(showsInfo -> showsInfo.getNumOfEpisodes()> 0)
                .map(this::mapShowInfoToResponseInfo)
                .collect(Collectors.toList()));



    }

    private ResponseInfo mapShowInfoToResponseInfo(ShowsInfo showsInfo){

        return modelMapper.map(showsInfo, ResponseInfo.class);
    }



}
