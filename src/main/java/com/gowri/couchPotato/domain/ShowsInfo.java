package com.gowri.couchPotato.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ShowsInfo implements Serializable {

    @JsonProperty("country")
    private String countryOfOrigin;

    private String description;

    @Value("${some.key:false}")
    private boolean drm;

    @PositiveOrZero
    @JsonProperty("episodeCount")
    @Value("${some.key:0}")
    private int numOfEpisodes;

    private String genre;

    @JsonProperty("image")
    private ImageInfo imageInfo;

    private String language;

    @JsonProperty("nextEpisode")
    private NextEpisodeInfo nextEpisode;

    private String primaryColour;

    @JsonProperty("seasons")
    private List<SeasonInfo> infoOnNextSeasons;

    @NotNull
    private String slug;

    @NotNull
    private String title;


    @NotNull
    private String tvChannel;

}
