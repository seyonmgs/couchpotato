package com.gowri.couchPotato.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.net.URL;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResponseInfo implements Serializable {

    @NotNull
    @JsonProperty("image")
    private URL showImage;

    @NotNull
    private String slug;

    @NotNull
    private String title;

}
