package com.gowri.couchPotato.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.swing.text.html.HTML;
import java.io.Serializable;
import java.net.URL;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NextEpisodeInfo implements Serializable {

    private String channel;
    private URL channelLogo;
    private LocalDate date;
    private String html;
    private URL url;
}
