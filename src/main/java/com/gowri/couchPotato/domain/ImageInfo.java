package com.gowri.couchPotato.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.net.URL;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ImageInfo implements Serializable {

    private URL showImage;
}
