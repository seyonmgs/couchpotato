package com.gowri.couchPotato.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.MessageSourceResourceBundle;

import java.util.ResourceBundle;

@Configuration
public class AppConfiguration {

    @Bean
    public ResourceBundle messageSource(){
       return MessageSourceResourceBundle.getBundle("messages");
    }


    @Bean
    public ModelMapper modelMapper() {return new ModelMapper();}
}
