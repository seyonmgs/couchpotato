package com.gowri.couchPotato.controller;

import com.gowri.couchPotato.dto.ResponseMessageDto;
import com.gowri.couchPotato.dto.RequestMessageDto;
import com.gowri.couchPotato.service.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class BaseController {

    @Autowired
    private AppService appService;

    @PostMapping("/")
    public ResponseMessageDto processPayload(@Valid @RequestBody RequestMessageDto requestMessageDto) {

        ResponseMessageDto responseMessageDto= appService.processRequest(requestMessageDto);
        return  responseMessageDto;
    }
}
