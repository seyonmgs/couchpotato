package com.gowri.couchPotato;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gowri.couchPotato.domain.ResponseInfo;
import com.gowri.couchPotato.dto.ErrorMessageDto;
import com.gowri.couchPotato.dto.RequestMessageDto;
import com.gowri.couchPotato.dto.ResponseMessageDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.util.Optional;
import java.util.function.Predicate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CouchPotatoApplicationTests {


    @LocalServerPort
    private int port;
    private TestRestTemplate restTemplate = new TestRestTemplate();
    private HttpHeaders headers = new HttpHeaders();


    @Test
    public void test1() {

        ResponseEntity<ResponseMessageDto> responseEntity=getResponseMessageEntity("src/test/inputJson1");

        ResponseMessageDto responseMessageDto =  responseEntity.getBody();

        Predicate<ResponseInfo> showImage1 = (responseInfo -> responseInfo.getShowImage().toString()
                .equals("http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg"));

        Predicate<ResponseInfo> slug1 = (responseInfo -> responseInfo.getSlug()
                .equals("show/scoobydoomysteryincorporated"));

        Predicate<ResponseInfo> slug2 = (responseInfo -> responseInfo.getSlug()
                .equals("show/seapatrol"));

        Predicate<ResponseInfo> title1 = (responseInfo -> responseInfo.getTitle()
                .equals("Two and a Half Men"));

        Assert.assertEquals(200, responseEntity.getStatusCodeValue());
        Assert.assertEquals(7, responseMessageDto.getResponseInfoList().size());
        Assert.assertEquals(1, getCountOf(showImage1, responseMessageDto));
        Assert.assertEquals("show/16kidsandcounting", getAnyResponseInfo(showImage1,responseMessageDto).getSlug());
        Assert.assertEquals("16 Kids and Counting", getAnyResponseInfo(showImage1,responseMessageDto).getTitle());

        Assert.assertEquals(1, getCountOf(slug1, responseMessageDto));
        Assert.assertEquals("http://mybeautifulcatchupservice.com/img/shows/ScoobyDoo1280.jpg"
                , getAnyResponseInfo(slug1, responseMessageDto).getShowImage().toString());
        Assert.assertEquals("Scooby-Doo! Mystery Incorporated", getAnyResponseInfo(slug1,responseMessageDto).getTitle());

        Assert.assertEquals(0, getCountOf(slug2, responseMessageDto));
        Assert.assertEquals(0, getCountOf(title1, responseMessageDto));
    }


    @Test
    public void test2(){
        /*
        The second item in the array is missing the element "slug". Hence parsing should fail and be caught by
        custom error handler.
         */
        ResponseEntity<ErrorMessageDto> errorEntity=getErrorMessageEntity("src/test/inputJson2");

        ErrorMessageDto errorMessageDto =  errorEntity.getBody();
        Assert.assertEquals(400, errorEntity.getStatusCodeValue());
        Assert.assertEquals("Could not decode request: JSON parsing failed" , errorMessageDto.getError());

    }

    @Test
    public void test3(){
        /*
        The first item in the array has a non-numeric value in episodeCount.
         */
        ResponseEntity<ErrorMessageDto> errorEntity=getErrorMessageEntity("src/test/inputJson3");

        ErrorMessageDto errorMessageDto =  errorEntity.getBody();
        Assert.assertEquals(400, errorEntity.getStatusCodeValue());
        Assert.assertEquals("Could not decode request: JSON parsing failed" , errorMessageDto.getError());

    }

    private ResponseInfo getAnyResponseInfo(Predicate<ResponseInfo> predicate, ResponseMessageDto responseMessageDto){

        Optional<ResponseInfo> optionalResponseInfo= responseMessageDto
                .getResponseInfoList()
                .stream()
                .filter(predicate)
                .findAny();

        ResponseInfo responseInfoItem = new ResponseInfo();

        if (optionalResponseInfo.isPresent()){
            responseInfoItem = optionalResponseInfo.get();
        }
        return  responseInfoItem;
    }

    private long getCountOf(Predicate<ResponseInfo> predicate, ResponseMessageDto responseMessageDto ){
        return responseMessageDto
                .getResponseInfoList()
                .stream()
                .filter(predicate)
                .count();
    }


    private ResponseEntity<ResponseMessageDto> getResponseMessageEntity (String filename){
        HttpEntity<RequestMessageDto> entity = buildRequestMessageEntity(filename);

        return restTemplate.exchange(
                buildURLWithPort("/"),
                HttpMethod.POST, entity, ResponseMessageDto.class);

    }

    private ResponseEntity<ErrorMessageDto> getErrorMessageEntity (String filename){
        HttpEntity<RequestMessageDto> entity = buildRequestMessageEntity(filename);

        return restTemplate.exchange(
                buildURLWithPort("/"),
                HttpMethod.POST, entity, ErrorMessageDto.class);

    }

    private HttpEntity<RequestMessageDto> buildRequestMessageEntity(String filename){
        RequestMessageDto requestMessageDto = buildRequestMessageDto(filename);

        return  new HttpEntity<>(requestMessageDto, headers);

    }
    private RequestMessageDto buildRequestMessageDto(String filename) {

        ObjectMapper mapper = new ObjectMapper();

        RequestMessageDto requestMessageDto = new RequestMessageDto();

        try {
            File file = new File(filename);
            requestMessageDto = mapper.readValue(file, RequestMessageDto.class);

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

        return requestMessageDto;
    }


    private String buildURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

}
