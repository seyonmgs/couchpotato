FROM openjdk:8
ADD target/couch-potato.jar couch-potato.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "couch-potato.jar"]